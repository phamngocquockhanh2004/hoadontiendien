
class KHACHHANGVIETNAM extends KHACHHANG {
    private String doiTuong;
    private double dinhMuc;

    public KHACHHANGVIETNAM(String maKhachHang, String hoTen, int ngay, int thang, int nam,
                             double soLuong, double donGia, String doiTuong, double dinhMuc) {
        super(maKhachHang, hoTen, ngay, thang, nam, soLuong, donGia);
        this.doiTuong = doiTuong;
        this.dinhMuc = dinhMuc;
    }

    @Override
    public double TinhThanhTien() {
        if (soLuong <= dinhMuc) {
            return soLuong * donGia;
        } else {
            return soLuong * donGia * dinhMuc + (soLuong - dinhMuc) * donGia * 2.5;
        }
    }
    
//    public boolean isKhachHangVN() {
//    	return true;
//    }
}