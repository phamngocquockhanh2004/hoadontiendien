
abstract class KHACHHANG implements ITIENDIEN {
    protected String maKhachHang;
    protected String hoTen;
    protected int ngay;
    protected int thang;
    protected int nam;
    protected double soLuong;
    protected double donGia;

    public KHACHHANG(String maKhachHang, String hoTen, int ngay, int thang, int nam, double soLuong, double donGia) {
        this.maKhachHang = maKhachHang;
        this.hoTen = hoTen;
        this.ngay = ngay;
        this.thang = thang;
        this.nam = nam;
        this.soLuong = soLuong;
        this.donGia = donGia;
    }

    @Override
    public abstract double TinhThanhTien();
    
//    public abstract boolean isKhachHangVN();
}