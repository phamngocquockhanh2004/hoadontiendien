import java.util.ArrayList;
import java.util.Scanner;

public class BAIKIEMTRA {
    public static void main(String[] args) {
        DSKHACHHANG dsKhachHang = new DSKHACHHANG();

        KHACHHANGVIETNAM khachVietNam = new KHACHHANGVIETNAM("VN001", "Nguyen Van A", 1, 1, 2023, 100, 5000, "Sinh hoạt", 50);
        KHACHHANGNUOCNGOAI khachNuocNgoai = new KHACHHANGNUOCNGOAI("NN001", "John Doe", 2, 1, 2023, 120, 6000, "USA");

        dsKhachHang.ThemKhachHang(khachVietNam);
        dsKhachHang.ThemKhachHang(khachNuocNgoai);

        // a) Thêm mới khách hàng.
        KHACHHANGVIETNAM khachVietNamMoi = new KHACHHANGVIETNAM("VN002", "Nguyen Van B", 3, 1, 2023, 80, 5500, "Kinh doanh", 30);
        dsKhachHang.ThemKhachHang(khachVietNamMoi);
//        dsKhachHang.ThemKhachHang(khachNuocNgoai);

        // b) Xóa khách hàng được chỉ định theo mã khách hàng
//        dsKhachHang.XoaKhachHang("VN001");

        // c) Cập nhật thông tin một khách hàng được chỉ định theo mã khách hàng
//        KHACHHANGNUOCNGOAI khachNuocNgoaiMoi = new KHACHHANGNUOCNGOAI("NN002", "Jane Doe", 4, 1, 2023, 150, 7000, "UK");
//        dsKhachHang.CapNhatKhachHang("NN001", khachNuocNgoaiMoi);

        // d) Tim kiếm khách hàng theo mà chỉ định
        KHACHHANG timKiemKhachHang = dsKhachHang.TimKiemKhachHang("VN002");
        if (timKiemKhachHang != null) {
            System.out.println("Khách hàng được tìm thấy: " + timKiemKhachHang.hoTen);
        } else {
            System.out.println("Khách hàng không tồn tại.");
        }

        // Tính tổng số lượng cho từng loại khách hàng
        dsKhachHang.TinhTongSoLuong();
    }
}
