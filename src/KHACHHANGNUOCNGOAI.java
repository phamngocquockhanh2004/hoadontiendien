
class KHACHHANGNUOCNGOAI extends KHACHHANG {
    private String quocTich;

    public KHACHHANGNUOCNGOAI(String maKhachHang, String hoTen, int ngay, int thang, int nam,
                              double soLuong, double donGia, String quocTich) {
        super(maKhachHang, hoTen, ngay, thang, nam, soLuong, donGia);
        this.quocTich = quocTich;
    }

    @Override
    public double TinhThanhTien() {
        return soLuong * donGia;
    }
    
//    @Override
//    public boolean isKhachHangVN() {
//    	return false;
//    }
}