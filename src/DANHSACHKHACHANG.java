import java.util.ArrayList;

class DSKHACHHANG {
    private ArrayList<KHACHHANG> danhSachKhachHang;

    public DSKHACHHANG() {
        danhSachKhachHang = new ArrayList<>();
    }

    public void ThemKhachHang(KHACHHANG khachHang) {
        danhSachKhachHang.add(khachHang);
    }

    public void XoaKhachHang(String maKhachHang) {
        danhSachKhachHang.removeIf(khachHang -> khachHang.maKhachHang.equals(maKhachHang));
    }

    public void CapNhatKhachHang(String maKhachHang, KHACHHANG khachHangMoi) {
        for (int i = 0; i < danhSachKhachHang.size(); i++) {
            if (danhSachKhachHang.get(i).maKhachHang.equals(maKhachHang)) {
                danhSachKhachHang.set(i, khachHangMoi);
                break;
            }
        }
    }

    public KHACHHANG TimKiemKhachHang(String maKhachHang) {
        for (KHACHHANG khachHang : danhSachKhachHang) {
            if (khachHang.maKhachHang.equals(maKhachHang)) {
                return khachHang;
            }
        }
        return null;
    }

    public void TinhTongSoLuong() {
        int khachHangVN = 0, khachHangNuocNgoai = 0;

        for (KHACHHANG khachHang : danhSachKhachHang) {
            if (khachHang instanceof KHACHHANGVIETNAM) {
                khachHangVN++;
            } else {
				khachHangNuocNgoai++;
			}
        }

        System.out.println("Tổng số lượng khách hàng Việt Nam: " + khachHangVN);
        System.out.println("Tổng số lượng khách hàng Nước ngoài: " + khachHangNuocNgoai);
    }
}